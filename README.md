# Ziva Vfx Utils #

These are utility scripts for the Ziva Vfx maya plugin.  When grabbing from here 
it is safest to grab the latest tagged release.  Untagged releases have not been 
tested as extensively and as a result you are more likely to encounter a bug.


Documents can be found here:
http://ziva-vfx.readthedocs.io/en/latest/


### Who do I talk to? ###

* support@zivadynamics.com OR
* lonniek@zivadynamics.com