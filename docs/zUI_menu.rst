zUI
===

A *beta* graphical UI to help navigate the Ziva setup in Maya.


.. image:: images/zUI.png


To run in Maya:

.. code-block:: python

    import zUI.ui as ui
    myWin = ui.ZivaUi()
    myWin.run()