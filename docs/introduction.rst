Introduction
============

Welcome to Ziva's VFX Utilities, a set of python utilities that have been designed
to help leverage the benefits of the Ziva Character Simulation paradigm.  You can find the BitBucket repo for these tools
`here <https://bitbucket.org/zivadynamics/ziva-vfx-utils>`_.

`Ziva Dynamics <http://zivadynamics.com>`_


:doc:`zBuilder_menu` is a tool for extracting part of a Maya scene into a Python representation
that’s easy to manipulate, save to disk, and re-apply to a Maya scene.
It has lots of support for manipulating Ziva creatures and can be easily extended.
It’s designed to be used by Technical Directors who are rigging characters with ZivaVFX.
It supports use cases such as updating a rig when new geometry is published and
transferring a Ziva rig from one character to another.

:doc:`zUI_menu` is a beta UI to help navigate the Ziva setup in Maya.
Among other things, it can help list objects in the simulation, find their attachments,
quickly navigate and select Ziva DG nodes, and drop into painting maps.
