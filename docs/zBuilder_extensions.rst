Extending zBuilder
==================

zBuilder is designed to be easily extended by adding new customized builders that handle specialized sets of nodes and parameters. It's also possible to add the ability to handle new types of nodes and parameters to the framework.

More to come....
